---
name: COVID-19
---

# COVID-19 Information (as of July 2022)

<br>

## Entry requirements

Since May 2022, Kosovo no longer imposes any COVID-19 related entry
requirements.

Airlines may have restrictions in place.

## Conference Policy

The DebConf22 organizers are enforcing a vaccination or regular testing
policy. Every attendee should be fully vaccinated, with the most recent
dose/booster within the last 12 months.

If you are not vaccinated, you will need to take a COVID-19 test every
48hrs.

We recommend attendees self-test before arrival, and every 2 days during
the conference.

## COVID-19 Testing

Rapid antigen self-tests are available in local pharmacies.
There will also be some available at the front desk.

There are PCR and rapid antigen tests available from several private
labs in Prizren, Prishtina and other major towns in Kosovo.
The main public hospital in Prishtina, the University Clinical Center of
Kosovo (UCCK), provides free travel testing without a referral although
availability and schedule depend on demand.

If you are in Kosovo and have COVID-19 related questions, are
symptomatic, or need information regarding testing, please call for free
038 200 80 800, or dial 194.

## Isolation on site

There will be rooms available for isolation, on site.

## Neighbouring countries COVID-19 information

Please see: [travel through neighbouring countries](/about/travel-through-neighbours/).
